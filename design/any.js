/*global mod*/
mod.constant('designAny', {
    "_id": "_design/any",
    "views": {
        "rev": {
            "map": "function(doc){ emit(doc._id, doc._rev)}"
        },
        "userposts": {
            "map": "function(doc){ if (doc.type=='post' && doc.createdBy != 'anonymous') {emit(doc.createdBy, 1)}}",
            "reduce": "_sum"
        }
    }
});