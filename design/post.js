/*global mod*/
mod.constant('designPost', {
    "_id": "_design/post",
    "views": {
        "allposts": {
            "map": "function(doc){ if (doc.type=='post') {emit(doc.id, [doc.title, doc.content, doc.createdOn, doc.createdBy, doc.editedOn, doc.editCount])}}",
            "reduce": "_count"
        },
        "allpostsregistered": {
            "map": "function(doc){ if (doc.type=='post' && doc.createdBy != 'anonymous') {emit(doc.id, [doc.title, doc.content, doc.createdOn, doc.createdBy, doc.editedOn, doc.editCount])}}",
            "reduce": "_count"
        },
        "titlecount": {
            "map": "function(doc){ if (doc.type=='post') {emit(doc.id, doc.title.length)}}",
            "reduce": "_sum"
        },
        "contentcount": {
            "map": "function(doc){ if (doc.type=='post') {emit(doc.id, doc.content.length)}}",
            "reduce": "_sum"
        }
    }
});