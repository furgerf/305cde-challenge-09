/*global mod*/
mod.constant('designUser', {
    "_id": "_design/user",
    "views": {
        "user": {
            "map": "function(doc){ if (doc.type=='user') {emit(doc.name, doc.password)}}",
            "reduce": "_count"
        }
    }
});