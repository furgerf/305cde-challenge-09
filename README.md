# Project for challenge 9 of 305cde (Developing the modern Web 2)

## Overview
The goal of this project is to implement a guestbook where users can leave messages. Users can log in or post anonymously but only logged in users can edit and delete (their own) posts. Furthermore, statistics about all the present posts and users can be viewed. This data is stored on a CouchDB which can be configured to point to any database that the user wishes.

## How it works
### Modules
Several modules are being used:
- Logger: logging utility which was developed as part of challenge 3 (shopping basket)
- Couch: handles database connection
- Post/User: provides functionality to implement post/user-behavior and serve as data contianers

### AngularJS
The different pages are displayed using AngularJS routing. Each page has a HTML-template with an Angular controller. The Angular-core is located in guestbook.js where services for those instances are created that are used across multiple controllers. Furthermore, this is where the RouteProvider is being configured.

All HTML-pages rely on Angular directives to display up-to-date data from their respective controllers by using two-way data binding.

Each controller is responsible for retrieving the data it displays and reacting to user input with it's controls.

### CouchDB
The user may choose a specific Couch database. The connection details to the database can be changed in the settings page. Once the settings are saved, the updated values take effect.

Upon initialization, the couch module executes a series of tests to see whether the database can be reached. If these tests succeed, queries to the database may be executed. Execution of queries happens with a job queue which ensures that all queries are executed but only after successful initialization.

If the couch host is reachable but the database does not exist, the database is created and the security document as well as the three design documents that are used are uploaded. These documents can be found in the JavaScript files under _/design_ where they're added to the Angular module as constants.

Queries are either to read (GET) or write (PUT) from the database. Write-access to update or delete documents require a read-access to "_design/any/_view/rev" with a specified key which is the document id (see below). Statistics on posts/users are generated using map/reduce-functions.

### Users/Posts
Initially, no user is logged in. Still, a _anonymous_ user can post but these posts cannot be edited or removed. Using the username/password field, a user can log in or create a new account. Once logged in, the user can log out again or delete his account. Logged in users can edit and delete their posts and get access to another page which displays an overview of all their own posts.

User passwords are never stored or transmitted in plain-text, always only their md5-hash. User and post objects are stored on CouchDB and therefore need an identity. In order to retrieve a specific document from the database, both modules implement a getId()-method which returns the md5-hash of a string which should never change for a post- or user-instance. For posts, it is the post creator and the post creation date and for users, it is the username. This reqiures that no 2 users with the same username can exist, which is being checked (and prevented) before user creation.

## Reflection
This is a fantastic challenge to "put it all together" what has been covered during the term: JavaScript, AJAX, AngularJS, map/reduce, NoSQL, CouchDB, Modules, completions, ... It also was the only challenge for me that reached a size that warranted thinking about file/directory structure.

The challenge shows the power and simplicity of using Angular's two-way data binding. It leaves the programmer free to think about how to actually gather the data without having to worry about updating the view which was a substantial concern in challenge 3 for example.

Some features could've been left out in favor of simplicity, like hashing passwords or supporting database authentication, but were implemented to be closer to a real word application.

Since a user's ID is based on his username and a post's ID is based on its creator's username and creation date, these three fields must be immutable. Since usernames are not allowed to change, that is not a problem. However, many systems nowadays allow their users to change their usernames so that would be a possible further extension. In order to make that work, the identity would have to be generated when the object is created and would have to be stored as a property of the object. That would require a substantial redesign but since it was not intended that users could change their names, I went with the approach I took because it is simpler.

Features like the posts paging for example should've been considered by the initial design. However, I only added that once it became clear that too many posts displayed at the same time would result in significantly reduced usability. On the other hand, editing posts has been taken into consideration from the beginning and didn't require unpleasant insertion late in the project.

A similar problem to posts paging was encountered with the user management. Since, at the time, I didn't know about Angular routing and how to effectively use multiple controllers, I integrated the user management in  the same controller together with the post management. This later had to be extracted and moved to the main controller. This was one of the many instances where the course layout was to my disadvantage.
