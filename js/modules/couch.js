/*global app*/

(function (window) {
    'use strict';

    function Couch($http, logger, settings, documents) {
        settings = settings || {};
        settings.couchHost = settings.couchHost || 'http://localhost';
        settings.couchPort = settings.couchPort || '5984';
        settings.couchDbName = settings.couchDbName || 'guestbook';

        var initialized = false,
            settingsChanged = (function () {
                // copies of the current settings to detect changes
                var lastHost = settings.couchHost,
                    lastPort = settings.couchPort,
                    lastDbName = settings.couchDbName,
                    lastUsername = settings.couchUser,
                    lastPassword = settings.couchPassword;

                return {
                    haveChanged: function () {
                        // check whether the settings have been changed
                        return settings.couchHost !== lastHost || settings.couchPort !== lastPort || settings.couchDbName !== lastDbName || settings.couchUser !== lastUsername || settings.couchPassword !== lastPassword;
                    },
                    setLast: function () {
                        // settings must have changed and been reacted to
                        // from now on, compare against current settings
                        lastHost = settings.couchHost;
                        lastPort = settings.couchPort;
                        lastDbName = settings.couchDbName;
                        lastUsername = settings.couchUser;
                        lastPassword = settings.couchPassword;
                    }
                };
            }()),
            jobQueue = [],
            getBareUrl = function () {
                // couchdb host
                // stored as a function rather than a variable since settings may change
                return settings.couchHost + ':' + settings.couchPort + '/';
            },
            getDbUrl = function () {
                // stored as a function rather than a variable since settings may change
                return getBareUrl() + settings.couchDbName + '/';
            },
            getConfig = function () {
                // $http-configuration for authentication
                // stored as a function rather than a variable since settings may change
                return {
                    withCredentials: true,
                    headers: {
                        'Authorization': 'Basic ' + window.btoa(settings.couchUser + ':' + settings.couchPassword)
                    }
                };
            },
            initComplete = function () {
                // initialization has been completed
                initialized = true;
                // execute any jobs that are waiting...
                while (jobQueue.length > 0) {
                    logger.logConsole("Executing job from jobQueue");
                    // remove job and execute it
                    jobQueue.splice(0, 1)[0]();
                }
            },
            init = function () {
                logger.logConsole("Initializing CouchDB-access");
                // check connection to CouchDB-server

                $http.get(getBareUrl())
                    .success(function (data, status, headers, config) {
                        // check connection to actual DB
                        logger.logConsole("Connection to CouchDB established successfully");
                        $http.get(getDbUrl(), getConfig())
                            .success(function (data, status, headers, config) {
                                logger.logConsole("Database located, initialization complete");
                                // conclude initialization
                                initComplete();
                            })
                            .error(function (data, status, headers, config) {
                                if (status === 404) {
                                    logger.logAlert("Database not found, attempting to create...");

                                    $http.put(getDbUrl(), "", getConfig())
                                        .success(function (data, status, headers, config) {
                                            logger.logConsole("Database created, uploading security document");

                                            $http.put(getDbUrl() + "_security", '{"admins": { "names": [], "roles": [] }, "members": { "names": ["' + settings.couchUser + '"], "roles": [] } }', getConfig())
                                                .success(function (data, status, headers, config) {
                                                    logger.logConsole("Security document uploaded, uploading design documents");
                                                    var designUploadCount = 0;

                                                    (documents || []).forEach(function (doc) {
                                                        $http.put(getDbUrl() + doc.url, doc.data, getConfig())
                                                            .success(function (data, status, headers, config) {
                                                                if (++designUploadCount === (documents || []).length) {
                                                                    logger.logAlert("Database creation complete!");
                                                                    // all design documents were uploaded, conclude initialization
                                                                    initComplete();
                                                                }
                                                            })
                                                            .error(function (data, status, headers, config) {
                                                                logger.logAlert("Error while uploading design document");
                                                                logger.logConsole(data);
                                                            });
                                                    });

                                                })
                                                .error(function (data, status, headers, config) {
                                                    logger.logAlert("Error whlie uploading security document?");
                                                    logger.logConsole(data);
                                                });
                                        })
                                        .error(function (data, status, headers, config) {
                                            logger.logAlert("Could not create database, maybe CORS is disabled?");
                                            logger.logConsole(data);
                                        });
                                } else if (status === 401) {
                                    logger.logAlert("Database login invalid, check your credentials!");
                                } else {
                                    logger.logAlert("Unknown error (" + status + ") while attempting to connec to database");
                                }
                            });
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("The host you are trying to access does not exist!");
                    });
            },
            addJob = function (job) {
                if (initialized) {
                    if (settingsChanged.haveChanged()) {
                        // at least one of the settings has changed, un-init
                        initialized = false;
                        // update "last"-stuff
                        settingsChanged.setLast();
                        // don't forget about the current job though!
                        addJob(job);
                        // re-init
                        logger.logConsole("Re-initializing CouchDB");
                        init();
                    } else {
                        logger.logConsole("Executing job directly");
                        job();
                    }
                } else {
                    if (settingsChanged.haveChanged()) {
                        // at least one of the settings has changes, re-init
                        // update "last"-stuff
                        settingsChanged.setLast();
                        // don't forget about the current job though!
                        addJob(job);
                        // re-init
                        logger.logConsole("Re-initializing CouchDB");
                        init();
                    } else {
                        logger.logConsole("Adding job to jobQueue");
                        jobQueue.push(job);
                    }
                }
            };

        // initialization
        init();

        // user functions
        this.getUser = function (name, completion) {
            logger.logConsole("Retrieving user with name " + name);

            // tries to retrieve user with *name* and feeds the results to the *completion*
            addJob(function () {
                logger.logConsole("JOB: getUser");
                $http.get(getDbUrl() + "_design/user/_view/user?reduce=false&key=\"" + name + "\"", getConfig())
                    .success(function (data, status, headers, config) {
                        completion(data.rows.length > 0 ? new app.User(data.rows[0].key, data.rows[0].value) : null);
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running getPosts:");
                        logger.logAlert(data);
                    });
            });
        };

        this.addUser = function (user) {
            logger.logConsole("Adding user " + user);

            // set db-specific property
            user.type = "user";

            // try to put data to db
            addJob(function () {
                logger.logConsole("JOB: addUser");
                $http.put(getDbUrl() + user.getId(), user, getConfig())
                    .success(function (data, status, headers, config) {
                        logger.logConsole("Successfully added user!");
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running addUser:");
                        logger.logAlert(data);
                    });
            });
        };

        this.removeUser = function (user, completion) {
            logger.logConsole("Removing user " + user);

            // tries to retrieve the user's revision
            addJob(function () {
                logger.logConsole("JOB: removeUser");
                $http.get(getDbUrl() + "_design/any/_view/rev?key=\"" + user.getId() + "\"", getConfig())
                    .success(function (data, status, headers, config) {
                        // tries to delete the user with the revision
                        $http.delete(getDbUrl() + data.rows[0].key + "?rev=" + data.rows[0].value, getConfig())
                            .success(function (data, status, headers, config) {
                                if (completion) {
                                    // run completion code after user has been deleted
                                    completion();
                                }
                            })
                            .error(function (data, status, headers, config) {
                                logger.logAlert("ERROR while running removeUser:");
                                logger.logAlert(data);
                            });
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running removeUser:");
                        logger.logAlert(data);
                    });
            });
        };

        // post functions
        this.getPosts = function (completion) {
            logger.logConsole("Retrieving posts");

            // gets all posts and feeds them to the *completion*
            addJob(function () {
                logger.logConsole("JOB: getPosts");
                $http.get(getDbUrl() + "_design/post/_view/allposts?reduce=false", getConfig())
                    .success(function (data, status, headers, config) {
                        // process received data: sort and parse
                        var posts = data.rows.map(function (rawPost) {
                            var newPost = new app.Post(rawPost.value[0], rawPost.value[1], rawPost.value[3], rawPost.value[2]);
                            newPost.editedOn = rawPost.value[4];
                            newPost.editCount = rawPost.value[5];
                            return newPost;
                        }).sort(function (a, b) {
                            var c = new Date(a.createdOn), // only sort by creation date, not by editing date
                                d = new Date(b.createdOn);
                            return c.getTime() > d.getTime();
                        });

                        // feed parsed posts to completion
                        completion(posts);
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running getPosts:");
                        logger.logAlert(data);
                    });
            });
        };

        this.addPost = function (post) {
            logger.logConsole("Adding post");

            // set db-specific property
            post.type = "post";

            // put new document on db
            addJob(function () {
                logger.logConsole("JOB: addPost");
                $http.put(getDbUrl() + post.getId(), post, getConfig())
                    .success(function (data, status, headers, config) {
                        logger.logConsole("Successfully added post!");
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running addPost:");
                        logger.logAlert(data);
                    });
            });
        };

        this.editPost = function (post) {
            logger.logConsole("Updating " + post);

            // set db-specific property
            post.type = "post";

            addJob(function () {
                logger.logConsole("JOB: editPost");
                // retrieve revision for the post
                $http.get(getDbUrl() + "_design/any/_view/rev?key=\"" + post.getId() + "\"", getConfig())
                    .success(function (data, status, headers, config) {
                        // put modified post on db (overwrite existing post)
                        $http.put(getDbUrl() + post.getId() + "?rev=" + data.rows[0].value, post, getConfig())
                            .success(function (data, status, headers, config) {
                                logger.logConsole("Successfully updated post!");
                            })
                            .error(function (data, status, headers, config) {
                                logger.logAlert("ERROR while running editPost:");
                                logger.logAlert(data);
                            });
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running removePost:");
                        logger.logAlert(data);
                    });
            });
        };

        this.deletePost = function (post) {
            logger.logConsole("Removing post " + post);

            addJob(function () {
                logger.logConsole("JOB: deletePost");
                // retrieve post revision
                $http.get(getDbUrl() + "_design/any/_view/rev?key=\"" + post.getId() + "\"", getConfig())
                    .success(function (data, status, headers, config) {
                        // delete post
                        $http.delete(getDbUrl() + data.rows[0].key + "?rev=" + data.rows[0].value, getConfig())
                            .success(function (data, status, headers, config) {
                                logger.logConsole("Successfully removed post");
                            })
                            .error(function (data, status, headers, config) {
                                logger.logAlert("ERROR while running removePost:");
                                logger.logAlert(data);
                            });
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running removePost:");
                        logger.logAlert(data);
                    });
            });
        };

        // map/reduce statistics
        this.getTotalPostCount = function (completion) {
            logger.logConsole("Getting total post count");

            addJob(function () {
                logger.logConsole("JOB: getTotalPostCount");
                $http.get(getDbUrl() + "_design/post/_view/allposts", getConfig())
                    .success(function (data, status, headers, config) {
                        if (completion) {
                            completion(data.rows[0] ? data.rows[0].value : 0);
                        }
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running getTotalPostCount:");
                        logger.logAlert(data);
                    });
            });
        };
        
        this.getTotalRegisteredPostCount = function (completion) {
            logger.logConsole("Getting total registered post count");

            addJob(function () {
                logger.logConsole("JOB: getTotalPostCount");
                $http.get(getDbUrl() + "_design/post/_view/allpostsregistered", getConfig())
                    .success(function (data, status, headers, config) {
                        if (completion) {
                            completion(data.rows[0] ? data.rows[0].value : 0);
                        }
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running getTotalRegisteredPostCount:");
                        logger.logAlert(data);
                    });
            });
        };
        
        this.getTotalTitleLength = function (completion) {
            logger.logConsole("Getting total title length");

            addJob(function () {
                logger.logConsole("JOB: getTotalTitleLength");
                $http.get(getDbUrl() + "_design/post/_view/titlecount", getConfig())
                    .success(function (data, status, headers, config) {
                        if (completion) {
                            completion(data.rows[0] ? data.rows[0].value : 0);
                        }
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running getTotalTitleLength:");
                        logger.logAlert(data);
                    });
            });
        };

        this.getTotalContentLength = function (completion) {
            logger.logConsole("Getting total content length");

            addJob(function () {
                logger.logConsole("JOB: getTotalContentLength");
                $http.get(getDbUrl() + "_design/post/_view/contentcount", getConfig())
                    .success(function (data, status, headers, config) {
                        if (completion) {
                            completion(data.rows[0] ? data.rows[0].value : 0);
                        }
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running getTotalContentLength:");
                        logger.logAlert(data);
                    });
            });
        };

        this.getTotalUserCount = function (completion) {
            logger.logConsole("Getting total post count");

            addJob(function () {
                logger.logConsole("JOB: getTotalPostCount");
                $http.get(getDbUrl() + "_design/user/_view/user", getConfig())
                    .success(function (data, status, headers, config) {
                        if (completion) {
                            completion(data.rows[0] ? data.rows[0].value : 0);
                        }
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running getTotalPostCount:");
                        logger.logAlert(data);
                    });
            });
        };

        this.getTopUsers = function (completion) {
            logger.logConsole("Getting top users");

            addJob(function () {
                logger.logConsole("JOB: getTopUsers");
                $http.get(getDbUrl() + "_design/any/_view/userposts?group=true", getConfig())
                    .success(function (data, status, headers, config) {
                        if (completion) {
                            completion(data.rows.map(function (entry) {
                                return {
                                    name: entry.key,
                                    postCount: entry.value
                                };
                            }).sort(function (a, b) {
                                return a.postCount < b.postCount;
                            }));
                        }
                    })
                    .error(function (data, status, headers, config) {
                        logger.logAlert("ERROR while running getTopUsers:");
                        logger.logAlert(data);
                    });
            });
        };
    }

    // Export to window
    window.app = window.app || {};
    window.app.Couch = Couch;
}(window));