/*global app*/

(function (window) {
    'use strict';

    function Post(postTitle, postContent, postCreatedBy, postCreatedOn) {
        this.title = postTitle || "";
        this.content = postContent || "";
        this.createdOn = postCreatedOn || new Date().toLocaleString();
        this.createdBy = postCreatedBy || "anonymous";
        this.editedOn = null;
        this.editCount = 0;

        this.editPost = function (newTitle, newContent) {
            this.title = newTitle;
            this.content = newContent;
            this.editCount = this.editCount + 1;
            this.editedOn = new Date().toLocaleString();
        };

        this.getId = function () {
            return app.MD5(this.createdBy + this.createdOn);
        };
    }

    // Export to window
    window.app = window.app || {};
    window.app.Post = Post;
}(window));