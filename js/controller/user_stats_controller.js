/*global mod*/

mod.controller('userStatsController', ['couch',
                                       function (couch) {
        'use strict';
        var that = this;

        couch.getTotalUserCount(function (result) {
            that.totalUsers = result;
            if (that.totalUsers !== 0) {
                that.averageUserPostCount = (that.totalRegisteredPosts / that.totalUsers).toFixed(3);
            } else if (that.totalRegisteredPosts === 0) {
                that.averageUserPostCount = 0;
            }
        });
        couch.getTotalRegisteredPostCount(function (result) {
            that.totalRegisteredPosts = result;
            if (that.totalUsers !== 0) {
                that.averageUserPostCount = (that.totalRegisteredPosts / that.totalUsers).toFixed(3);
            } else if (that.totalRegisteredPosts === 0) {
                that.averageUserPostCount = 0;
            }
        });

        couch.getTopUsers(function (result) {
            that.topUsers = result;
        });
    }]);