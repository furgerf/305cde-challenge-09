/*global app, confirm, mod*/
mod.controller('ownPostsController', ['logger', 'couch', 'currentUser', '$location',
                                function (logger, couch, currentUser, $location) {
        'use strict';
        // continuous access to caller and some Important Objects
        var that = this;

        // initialization - check whether someone is logged in
        if (currentUser.name) {
            // load posts
            // pass handling of retrieved data as completion
            couch.getPosts(function (result) {
                that.posts = result.filter(function (post) {
                    return post.createdBy === currentUser.name;
                });
            });
        } else {
            // go home
            logger.logConsole("No user is logged in (page refresh?), re-directing to home");
            $location.url('/');
        }
    }]);