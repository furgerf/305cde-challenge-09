/*global mod*/

mod.controller('postStatsController', ['couch',
    function (couch) {
        'use strict';
        var that = this;

        couch.getTotalPostCount(function (result) {
            that.totalPosts = result;
            if (that.totalPosts !== 0) {
                that.averageTitleLength = (that.totalTitleLength / that.totalPosts).toFixed(3);
                that.averageContentLength = (that.totalContentLength / that.totalPosts).toFixed(3);
            } else {
                if (that.totalTitleLength === 0) {
                    that.averageTitleLength = 0;
                }
                if (that.totalContentLength === 0) {
                    that.averageContentLength = 0;
                }
            }
        });

        couch.getTotalTitleLength(function (result) {
            that.totalTitleLength = result;
            if (that.totalPosts !== 0) {
                that.averageTitleLength = (that.totalTitleLength / that.totalPosts).toFixed(3);
            } else if (that.totalPosts === 0) {
                that.averageTitleLength = 0;
            }
        });

        couch.getTotalContentLength(function (result) {
            that.totalContentLength = result;
            if (that.totalPosts !== 0) {
                that.averageContentLength = (that.totalContentLength / that.totalPosts).toFixed(3);
            } else if (that.totalPosts === 0) {
                that.averageContentLength = 0;
            }
        });
    }]);