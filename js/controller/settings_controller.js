/*global mod*/

mod.controller('settingsController', ['settings', 'logger',
                                       function (settings, logger) {
        'use strict';
        var that = this;
        that.couch = {};

        // bind service instance to model
        that.couch.host = settings.couchHost;
        that.couch.port = settings.couchPort;
        that.couch.dbName = settings.couchDbName;
        that.couch.username = settings.couchUser;
        that.couch.password = settings.couchPassword;

        // 'bind' model to service instance
        that.saveSettings = function () {
            settings.couchHost = that.couch.host;
            settings.couchPort = that.couch.port;
            settings.couchDbName = that.couch.dbName;
            settings.couchUser = that.couch.username;
            settings.couchPassword = that.couch.password;

            logger.logAlert("The settings have been saved!");
        };
    }]);