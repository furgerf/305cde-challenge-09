/*global angular, app*/

var mod = angular.module('guestbook', ['ngRoute']);

// services
mod.service('logger', [

    function () {
        // logger singleton instance
        'use strict';
        return new app.Logger(0xFF);
    }]);
mod.value('settings', {
    couchHost: "http://ffurger.iriscouch.com",
    couchPort: 5984,
    couchDbName: "guestbook",
    couchUser: "fabian",
    couchPassword: "password"
});
mod.service('couch', ['$http', 'logger', 'settings', 'designAny', 'designPost', 'designUser',
    function ($http, logger, settings, designAny, designPost, designUser) {
        // couch singleton instance
        // passes the injected design documents as an array to couch
        'use strict';
        return new app.Couch($http, logger, settings, [{
            url: "_design/any",
            data: designAny
        }, {
            url: "_design/post",
            data: designPost
        }, {
            url: "_design/user",
            data: designUser
        }]);
    }]);
mod.service('currentUser', [

    function () {
        // current user singleton instance
        'use strict';

        return new app.User();
    }]);

// routing configuration
mod.config(['$routeProvider',
        function ($routeProvider) {
        'use strict';

        $routeProvider
            .when('/posts', {
                templateUrl: 'html/all_posts.html',
                controller: 'allPostsController as allCtrl'
            })
            .when('/ownposts', {
                templateUrl: 'html/own_posts.html',
                controller: 'ownPostsController as ownCtrl'
            })
            .when('/poststats', {
                templateUrl: 'html/post_stats.html',
                controller: 'postStatsController as psCtrl'
            })
            .when('/userstats', {
                templateUrl: 'html/user_stats.html',
                controller: 'userStatsController as usCtrl'
            })
            .when('/settings', {
                templateUrl: 'html/settings.html',
                controller: 'settingsController as setCtrl'
            })
            .otherwise({
                redirectTo: '/posts'
            });
    }]);